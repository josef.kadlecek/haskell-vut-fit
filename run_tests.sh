
#Compile program
ghc -o plg-2-nka plg-2-nka.hs >/dev/null 2>/dev/null
if [ $? != 0 ] 
then
    echo "COMPILATION FAILED"
    exit 1
fi

OPTIONS=("-i" "-1" "-2")

#Funcion to run program with given parameters 
function run_single {
	./plg-2-nka $OPT $IN_FILE > testing.out

	DIFF=$(diff testing.out $REF_FILE) 

	if [ "$DIFF" != "" ] 
	then
	    echo "TEST $IN_FILE with option $OPT FAILED"
	    echo $DIFF
	else
		echo "TEST $IN_FILE with option $OPT PASSED"
	fi

	rm testing.out
}

function run_all {
	TESTNUM=1
	for TESTNUM in 1 2
	do
		for OPT in "${OPTIONS[@]}"
		do
			IN_FILE="./test$TESTNUM/test.in"
			REF_FILE="./test$TESTNUM/test$OPT.out"
			run_single
		done
	done

}

run_all
