# FLP - Haskell project #


### What is this repository for? ###

* School project in Haskel realizing algorithm for creating Nondeterministic end-state machine from right linear gramathics.


### How do I get set up? ###

* Have haskell.
* Compile (example): ghc -o plg-2-nka plg-2-nka.hs
* Run with: ./plg-2-nka -opt [file] 
* Options: -i, -1 and -2
* If file is not specified, reads stdin. 


### Testing ###

* Run ./run_tests.sh which compares output to ref output.

### Who do I talk to? ###

* Repo owner or admin - josef.kadlecek@gmail.com