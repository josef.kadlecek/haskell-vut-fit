--2plg-2nka: FLP 2020 Josef Kadlecek (xkadle35).
import System.IO
import Data.List
import System.Environment
import Data.Map (Map, fromList)
import qualified Data.Map as Map

--Returns number representing how many digits index has
indexPositions :: Show a => a -> Int
indexPositions index = length(show index) 
 
 --Returns right side of the rule
ruleRightSide :: String -> String
ruleRightSide rule = split '>' rule !! 1

-- Returns left side of the rule.
ruleLeftSide :: String -> String
ruleLeftSide rule = head (split '-' rule)

--Checks if list contains only terminals
onlyTerminals :: (Eq a) => [a] -> [a] -> Bool
onlyTerminals [] terminals = True
onlyTerminals (x:xs) terminals
  | x `elem` terminals = onlyTerminals xs terminals
  | otherwise = False

--Checks if rule is terminal
isTerminalRule :: t1 -> t -> String -> Bool
isTerminalRule terminals nonterminals rule
  | rightSide == "#" = True
  | otherwise = False
  where rightSide = ruleRightSide rule

--Check if rule is simple (from step one in algorithm)
isSimpleRule :: Eq a =>  [a] ->   [a] -> [a] -> Bool
isSimpleRule terminals nonterminals rule
  | ruleLength == 5 &&(thirdChar `elem` terminals) &&  ((rule !! 4) `elem` nonterminals) = True
  | otherwise = False
  where thirdChar = rule !! 3
        ruleLength = length rule 

--Check if rule contains string of terminals and ends with nonterminal (step two)
endsWithNonterminal :: Eq a =>  [a]-> [a] -> [a] -> Bool
endsWithNonterminal terminals nonterminals rule
  | length rule < 6 = False
  | not (onlyTerminals (init(drop 3 rule)) terminals) = False
  | otherwise = endsWithNonTerminal
  where endsWithNonTerminal = (rule !! (length rule -1)) `elem` nonterminals

--Check if rule contains only string of terminals (step three)
matchesrulesWithOnlyTerminals ::  [Char] -> t -> String -> Bool
matchesrulesWithOnlyTerminals terminals nonterminals rule
  | onlyTerminals (ruleRightSide rule) terminals = True
  | otherwise = False

--Creates new rule from left side of old rule
extractLeftmostRule :: Show a => String -> a -> String
extractLeftmostRule rule index= head (split '>' rule) ++ take 2 (split '-' rule !! 1) ++ show index

--Removes rule which has been degenerated alerday
removePreviousRule :: (Show a, Num a) => a -> String -> String
removePreviousRule index previousRule = show index ++ "->" ++ tail(ruleRightSide previousRule)

--From one complex rule creates many more simple rules (step 2 and 3)
degenarete :: (Show t1, Num t1) => String -> t1 ->  [Char] -> [String]
degenarete rule index terminals
  | ( rightSideLength == 2) && endsWithNonTerminal = [rule]
  | rightSideLength == 1 = (rule ++ show index) : [show index ++ "->#"]
  | otherwise = extractLeftmostRule rule index :
  degenarete (removePreviousRule index rule) (index+1) terminals
  where rightSideLength = length (ruleRightSide rule)
        endsWithNonTerminal = last rule `notElem` terminals
        indexDigits = indexPositions index

--Applyes degenerate on all complex rules
degenerateAll :: [String] -> Int -> [Char] -> [String]
degenerateAll [] index terminals = []
degenerateAll (x:xs) index terminals= degenarete x index terminals ++ 
                              degenerateAll xs (index + indexIncrease) terminals
  where ruleLength = length x
        indexIncrease = if last x `elem` terminals 
                        then ruleLength - 3  else ruleLength - 5

--Stolen from StackOwerflow. It just works. Splits string by char.
split :: Eq a => a -> [a] -> [[a]]
split d [] = []
split d s = x : split d (drop 1 y) where (x,y) = span (/= d) s

--Removes , from list.
removeCols :: String -> String
removeCols xs = [ x | x <- xs, x `notElem` ","]

--Removes " from list.
removeQots :: String -> String
removeQots xs = [ x | x <- xs, x `notElem` "\""]

--Returns index of first occurance of char in list.
getIndex :: (Eq a1, Num a) => a1 -> [a1] -> a -> a
getIndex char [] index = error "Element not in list. Was Nonterminal specified in list of NTs?"
getIndex char list index = if head list == char then index 
                                  else getIndex char (tail list) (index+1)

--Changes A-Z for representing numbers.
swapAlphaForNum :: String -> String -> String
swapAlphaForNum ntNoCols str
  | null str = ""
  | head str `elem` ['A'..'Z'] = show(getIndex (head str) ntNoCols 0) ++ swapAlphaForNum ntNoCols (tail str)
  | otherwise = head str : swapAlphaForNum ntNoCols (tail str)

rutine :: String -> String -> IO ()
rutine input option = do 
  let inputLines = lines input
  let nonterminals = head inputLines
  let terminals = inputLines !! 1
  let startingNonterminal = inputLines !! 2
  let rules = tail (tail (tail inputLines))
  let ntNoCols = removeCols nonterminals

  if option == "-i"
    then outputChoiceI terminals nonterminals startingNonterminal rules
  else return ()

  let terminalRules = filter (isTerminalRule terminals nonterminals) rules 
  let simpleRules = filter (isSimpleRule terminals nonterminals) rules

  let rulesWithNonTerminal = filter (endsWithNonterminal terminals nonterminals) rules
  let rulesWithOnlyTerminals = filter (matchesrulesWithOnlyTerminals terminals nonterminals) rules

  if sort (terminalRules++simpleRules++rulesWithNonTerminal++rulesWithOnlyTerminals) == sort rules
    then putStr ""  else error "Probrem with some rules."

  --Swap big alhabet character for numbers because we need to.
  let nonterminalsNum = swapAlphaForNum ntNoCols nonterminals
  let startingNonterminalNum = swapAlphaForNum ntNoCols startingNonterminal
  let simpleRulesNum = map (swapAlphaForNum ntNoCols) simpleRules
  let rulesWithNonTerminalNum = map (swapAlphaForNum ntNoCols) rulesWithNonTerminal 
  let rulesWithOnlyTerminalsNum = map (swapAlphaForNum ntNoCols)rulesWithOnlyTerminals
  let terminalRulesNum = map (swapAlphaForNum ntNoCols) terminalRules

  let rulesToDegenerate = rulesWithNonTerminalNum ++ rulesWithOnlyTerminalsNum
  let degenaretedRules = degenerateAll rulesToDegenerate (length ntNoCols) terminals
  let lastState = read (ruleLeftSide (last degenaretedRules))

  if option == "-1"
    then
    outputChoiceOne terminals nonterminalsNum lastState startingNonterminalNum simpleRulesNum terminalRulesNum degenaretedRules
  else
    return () 

  let newTerminalRules = filter (isTerminalRule terminals nonterminalsNum) degenaretedRules

  let endStates = (terminalRulesNum ++  newTerminalRules)

  let transitions =  simpleRulesNum ++ (degenaretedRules \\ newTerminalRules) 

  if option == "-2"
    then
    outputChoiceTwo nonterminalsNum lastState startingNonterminalNum endStates transitions
  else
    return ()

main = do
  args <- getArgs
  let option = head args

  if length args == 2 then do
    input <- readFile (args !! 1)
    rutine input option
    return()
  else if length args == 1 then do
    input <- getContents
    rutine input option
    return()
  else
    putStrLn "Unexpected number of arguments. Expected: -i|-1|-2 [filename]."


--Printing functions--
outputChoiceI :: String -> String -> String ->  [String] -> IO ()
outputChoiceI terminals nonterminals startingNonterminal rules = do
  putStrLn nonterminals 
  putStrLn terminals 
  putStrLn startingNonterminal 
  mapM_ putStrLn rules

outputChoiceOne :: (Show a, Num a, Enum a) => String
     -> t -> a -> String -> [String] -> [String] -> [String] -> IO ()
outputChoiceOne terminals nonterminals lastState s simpleRules terminalRules degenaretedRules= do
  printListWithABeforeNum (map (("A" ++) . show) [0 .. lastState])
  putStrLn terminals 
  putStrLn ("A" ++ s)
  mapM_ (putStrLn . addABeforeEveryNumberInRule) simpleRules
  mapM_ (putStrLn . addABeforeEveryNumberInRule) terminalRules
  mapM_ (putStrLn . addABeforeEveryNumberInRule) degenaretedRules


outputChoiceTwo:: (Show a, Num a, Enum a) =>
     String -> a -> String -> [String] -> [String] -> IO ()
outputChoiceTwo nonTerminals lastState startState endStates transitions=do
  printList [0..lastState]
  putStrLn startState
  printEndStates endStates
  printRules transitions

printEndStates :: [String] -> IO ()  
printEndStates [] = putStrLn ""
printEndStates [x] = putStrLn (ruleLeftSide x)
printEndStates (x:xs) = do
  putStr (ruleLeftSide x ++ ",")
  printEndStates xs

printRules :: [String] -> IO ()
printRules [] = putStr ""
printRules (rule:xs) = do
  putStr (ruleLeftSide rule ++ ",")
  let middleChar = head (ruleRightSide rule)
  putStr  (middleChar : ",")
  putStrLn (split middleChar rule !! 1)
  printRules xs

printList :: Show a => [a] -> IO ()
printList [x] = print x
printList (x:xs) = do
  putStr (show x ++ ",")
  printList xs

--Adds A before num in list.
printListWithABeforeNum :: [String] -> IO ()
printListWithABeforeNum [x] = putStrLn (removeQots x) 
printListWithABeforeNum (x:xs) = do
  putStr (removeQots x ++ ",")
  printListWithABeforeNum xs

--Adds A before every number in rule. DOnt want to, but have to.
addABeforeEveryNumberInRule :: String -> String
addABeforeEveryNumberInRule rule = "A" ++ lside ++ "->" ++ [sign] ++ addAorEndRule ++ rest 
  where lside = ruleLeftSide rule
        rside = ruleRightSide rule
        sign  = head rside
        rest = tail rside
        addAorEndRule = if  rside == "#" then "" else "A"